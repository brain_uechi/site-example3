module.exports = {
  syntax: "scss",
  plugins: ['stylelint-scss'],
  extends: ['stylelint-config-recommended-scss'],
  rules: {
    'no-empty-source': null,
    'block-no-empty': null,
    'string-quotes': 'single',
    'no-descending-specificity': null,
    'font-family-no-missing-generic-family-keyword': null,
  },
};
