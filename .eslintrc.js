module.exports = {
  root: true,
  env: {
    es6: true,
    node: true,
  },
  plugins: ['node', 'babel'],
  extends: [
    'eslint:recommended',
  ],
  parser: 'babel-eslint',
  parserOptions: {
    sourceType: 'module',
  },
  rules: {
    'max-len': 0,
    'no-console': 0,
    'no-var': 2,
    'no-unused-vars': 1,
    'quotes': [2, 'single'],
    'eol-last': 2,
    'comma-dangle': [2, 'always-multiline'],
    'semi': [2, 'always'],
    'operator-linebreak': [2, 'before'],
    // 'indent': [1, 2],
    'arrow-parens': [1, 'as-needed'],
    // 'arrow-body-style': [1, 'as-needed'],
  },
};
