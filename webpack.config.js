const TerserPlugin = require('terser-webpack-plugin');
const isProd = process.env.NODE_ENV === 'production';
module.exports = {
  module: {
    rules: [
      {
        enforce: 'pre',
        test: /\.js$/,
        exclude: /[\\/]node_modules[\\/]/,
        loader: 'eslint-loader',
        // options: {
        //   configFile: 'src/.eslintrc.js',
        // },
      },
      {
        test: /\.js$/,
        exclude: /[\\/]node_modules[\\/]/,
        loader: 'babel-loader',
      },
    ],
  },
  output:{
    pathinfo: false,
  },
  optimization:{
    minimize: true,
    minimizer: [
      new TerserPlugin({
        sourceMap: !isProd,
        terserOptions: {
          compress: {
            drop_console: isProd,
          },
        },
      }),
    ],
  },
  plugins: [],
  resolve: {
    extensions: ['.js'],
  },
};
