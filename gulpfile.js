const gulp = require('gulp');
const browserSync = require('browser-sync').create();
const webpackCompiler = require('webpack');

let useSourceMaps = true;
if (!process.env.NODE_ENV) process.env.NODE_ENV = 'development';

/**
 * env task
 */
gulp.task('env:dev', function (callback) {
  process.env.NODE_ENV = 'development';
  useSourceMaps = true;

  console.log('##############################\n'
    + '## NODE_ENV: development\n'
    + '## useSourceMap: ture\n'
    + '##############################');

  callback();
});
gulp.task('env:prod', function (callback) {
  process.env.NODE_ENV = 'production';
  useSourceMaps = false;

  console.log('##############################\n'
    + '## NODE_ENV: production\n'
    + '## useSourceMap: false\n'
    + '##############################');

  callback();
});

/**
 * browser-sync task
 */
gulp.task('serve', function (callback) {
  const config = require('./bs-config');
  browserSync.init(config, () => {
    callback();
  });
});

/**
 * browser-sync reload task
 */
gulp.task('serve:reload', function (callback) {
  browserSync.reload();
  callback();
});

/**
 * html lint task
 */
gulp.task('html:lint', function () {
  const htmlhint = require('gulp-htmlhint');

  return gulp.src([
    'htdocs/**/*.html',
  ], {
    base: 'htdocs',
    since: gulp.lastRun('html:lint'),
  })
  .pipe(htmlhint('.htmlhintrc'))
  .pipe(htmlhint.failAfterError());
});

/**
 * html watch task
 */
gulp.task('html:watch', function (callback) {
  gulp.watch([
    'htdocs/**/*.html',
  ], gulp.series('html:lint'));
  callback();
});

/**
 * css lint task
 */
gulp.task('css:lint', function () {
  const stylelint = require('gulp-stylelint');

  return gulp.src([
    'htdocs/**/*.scss',
  ], {
    base: 'htdocs',
    since: gulp.lastRun('css:lint'),
  })
  .pipe(stylelint({
    failAfterError: true,
    reporters: [
      {
        formatter: 'string',
        console: true,
      },
    ],
  }));
});

/**
 * js lint task
 */
gulp.task('js:lint', function () {
  const eslint = require('gulp-eslint');

  return gulp.src([
    'htdocs/**/js/**/*.js',
    '!htdocs/**/js/**/*.min.js',
  ], {
    base: 'htdocs',
    since: gulp.lastRun('js:lint'),
  })
  .pipe(eslint())
  .pipe(eslint.format())
  .pipe(eslint.failAfterError());
});

/**
 * css task
 */
gulp.task('css', function () {
  const sass = require('gulp-sass');
  const cached = require('gulp-cached');
  const progeny = require('gulp-progeny');
  const postcss = require('gulp-postcss');
  const autoprefixer = require('autoprefixer');
  const cssnano = require('cssnano');
  const rename = require('gulp-rename');
  const through = require('through2');

  return gulp.src([
    'htdocs/**/*.scss',
  ], {
    base: 'htdocs',
    sourcemaps: useSourceMaps,
  })
  .pipe(cached('scss'))
  .pipe(progeny())
  .pipe(sass().on('error', sass.logError))
  .pipe(postcss([
    autoprefixer({grid: true}),
    cssnano(),
  ]))
  .pipe(through.obj(function (file, enc, cb) {
    // append-buffer 対策
    file.contents = Buffer.from(file.contents.toString() + '\n');
    this.push(file);
    cb();
  }))
  .pipe(rename({extname: '.min.css'}))
  .pipe(gulp.dest('htdocs', {
    sourcemaps: '.',
  }))
  .pipe(browserSync.stream());
});

/**
 * css watch task
 */
gulp.task('css:watch', function (callback) {
  gulp.watch([
    'htdocs/**/*.scss',
  ], gulp.series('css:lint', 'css'));
  callback();
});

/**
 * js task
 */
gulp.task('js', function () {
  const webpackStream = require('webpack-stream');
  const named = require('vinyl-named');
  const webpackConfig = require('./webpack.config.js');
  webpackConfig.mode = process.env.NODE_ENV;
  webpackConfig.devtool = useSourceMaps ? 'source-map' : false;

  console.dir(webpackConfig);

  return gulp.src([
    'htdocs/**/js/*.js',
    '!htdocs/**/js/*.min.js',
  ], {
    base: 'htdocs',
    sourcemaps: useSourceMaps,
  })
  .pipe(named(function (file) {
    return file.relative.replace(/\.js$/, '.min');
  }))
  .pipe(webpackStream(webpackConfig, webpackCompiler))
  .pipe(gulp.dest('htdocs', {
    sourcemaps: '.',
  }))
  .pipe(browserSync.stream());

});

/**
 * js watch task
 */
gulp.task('js:watch', function (callback) {
  gulp.watch([
    'htdocs/**/js/*.js',
    '!htdocs/**/js/*.min.js',
  ], gulp.series('js'));
  callback();
});

/**
 * build clean task
 */
gulp.task('build:clean', function (callback) {
  const path = require('path');
  const glob = require('glob');
  const fs = require('fs-extra');
  const files = glob.sync('htdocs/**/*.map', {nodir: true});
  files.forEach(file => {
    let p = path.resolve(file);
    fs.removeSync(p);
    console.log(`delete ${p}`);
  });
  callback();
});


/**
 * default tasks
 */
gulp.task('default', gulp.series('env:dev', 'html:watch', 'css:watch', 'js:watch', 'css', 'js', 'serve'));
gulp.task('default-prod', gulp.series('env:prod', 'html:watch', 'css:watch', 'js:watch', 'css', 'js', 'serve'));
gulp.task('lint', gulp.series('html:lint', 'css:lint', 'js:lint'));
gulp.task('build', gulp.series('env:prod', 'build:clean', 'html:lint', 'css:lint', 'css', 'js'));
